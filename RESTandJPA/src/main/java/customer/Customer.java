package customer;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
public class Customer  {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
  @Column(name = "first_name", nullable = false)//, unique = true)*/
    private String firstName;
    @NotEmpty(message = "Last name is required")
    private String lastName;

    protected Customer() {}

    public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, firstName='%s', lastName='%s']",
                id, firstName, lastName);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!(o instanceof Customer))
            return false;

        Customer other = (Customer) o;

        return this.getId() == other.getId();
    }


    public String toJsonString(Customer customer){


        Gson gson = new Gson();
        /*JsonObject json = new JsonObject();
        json.addProperty("id",customer.getId());
        json.addProperty("firstName",customer.getFirstName());
        json.addProperty("lastName",customer.getLastName());*/

        return  gson.toJson(customer);

    }
}