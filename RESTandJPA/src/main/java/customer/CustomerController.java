package customer;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by internship on 11.07.2016.
 */

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<Customer>> getCustomers() {

        return ResponseEntity.ok((List<Customer>) customerRepository.findAll());
    }

    @RequestMapping("/{id}")
    public ResponseEntity<Customer> getCustomer(@PathVariable("id") long id) {

        if (customerRepository.findOne(id) != null)
            return ResponseEntity.ok(customerRepository.findOne(id));

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Customer> updateCustomer(@PathVariable("id") long id, @RequestBody Customer customer) {

        customer.setId(id); //GRESIT
        if (customerRepository.findOne(id) != null) {
            return ResponseEntity.ok(customerRepository.save(customer));
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity createCustomer(@Valid @RequestBody Customer customer) {

        if (customerRepository.findByFirstName(customer.getFirstName()).size() > 0) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();

        }
        Customer saved = customerRepository.save(customer);
        return ResponseEntity.status(HttpStatus.CREATED).body(saved);


    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteCustomer(@PathVariable("id") long id) {

        boolean found = customerRepository.exists(id);

        if (found) {
            customerRepository.delete(id);
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


}
