import customer.Application;
import customer.Customer;
import customer.CustomerController;
import customer.CustomerRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Application.class})
@WebIntegrationTest
public class CustomerControllerTest {
    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MockMvc mockMvc;

    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    CustomerController customerController;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setUp() {

        this.mockMvc = standaloneSetup(customerController)
                .defaultRequest(get("/")
                        .accept(MediaType.APPLICATION_JSON))
                .build();
    }

    @After
    public void tearDown() {

        customerRepository.deleteAll();
    }


    @Test
    public void getCustomers() throws Exception {

        //EXECUTE
        mockMvc.perform(get("/customers/list").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getCustomer() throws Exception {

        //PREPARE
        Customer customer = customerRepository.save(new Customer("Kim", "Bauer"));
        long id = customer.getId();
        Customer c = new Customer("Kim", "Bauer");
        c.setId(id);
        String jsonObj = c.toJsonString(c);
        String resultActionString;

        //EXECUTE
        ResultActions resultActions = mockMvc.perform(get("/customers/{0}", id).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());


        //VERIFY
        resultActionString = resultActions.andReturn().getResponse().getContentAsString();
        Assert.assertEquals(jsonObj.toString(), resultActionString);
    }


    @Test
    public void getCustomerIdNotFound() throws Exception {

        //EXECUTE
        mockMvc.perform(get("/customers/{0}", 101L).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent());

    }


    @Test
    public void postCustomer() throws Exception {

        //prepare
        Customer customer = new Customer("Mathias", "Bob");

        //execute
        this.mockMvc.perform(post("/customers/")
                .contentType(contentType)
                .content(json(customer)))
                .andExpect(status().isCreated());
    }

    @Test
    public void postExistingCustomer() throws Exception {

        //prepare
        long id;
        Customer customer = customerRepository.save(new Customer("Kim", "Bauer"));
        Customer customer2 = new Customer("Kim", "Bauer");
        id = customer.getId();
        customer2.setId(id);

        //EXECUTE
        this.mockMvc.perform(post("/customers/")
                .contentType(contentType)
                .content(json(customer)))
                .andExpect(status().isConflict());
    }

    @Test
    public void updateCustomer() throws Exception {

        //PREPARE
        Customer customer = customerRepository.save(new Customer("Kim", "Bauer"));
        long id = customer.getId();
        Customer c = new Customer("Kim-Marvin", "Bauer");
        c.setId(id);

        //EXECUTE
        this.mockMvc.perform(put("/customers/{0}", id)
                .contentType(contentType)
                .content(json(customer)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void updateNonExistingCustomer() throws Exception {
        //PREPARE
        Customer customer = new Customer("Jack-M", "Bauer");

        //EXECUTE
        this.mockMvc.perform(put("/customers/{0}", 100)
                .contentType(contentType)
                .content(json(customer)))
                .andDo(print())
                .andExpect(status().isNoContent());
    }


    @Test
    public void deleteCustomer() throws Exception {

        //PREPARE
        Customer customer = customerRepository.save(new Customer("Kim", "Bauer"));
        long id = customer.getId();

        //EXECUTE
        this.mockMvc.perform(delete("/customers/{0}", id))
                .andDo(print())
                .andExpect(status().isNoContent());

    }

    @Test
    public void deleteNonExistingCustomer() throws Exception {

        //EXECUTE
        this.mockMvc.perform(delete("/customers/{0}", 100))
                .andDo(print())
                .andExpect(status().isNotFound());

    }


    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }


}
